from setuptools import setup, find_packages


setup(
    name='dadajokes',
    version='0.1.0',
    description='A Python package for getting random dad jokes',
    author='Innocent Kithinji',
    author_email='innocent@ikithinji.com',
    license='MIT',
    packages=find_packages(),
    classifiers=["Python:3.9", "Demo"],
    python_requires='>=3.6',
)